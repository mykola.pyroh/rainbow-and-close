function partColorRandom() {
    return Number((Math.random()*256).toFixed());
}

function changeColor() {
    document.querySelector('body').style.backgroundColor = '#'.concat(partColorRandom().toString(16), partColorRandom().toString(16), partColorRandom().toString(16));
    console.log('Vyvid')
}

setInterval(changeColor, 2000);
setTimeout(window.close, 15000); // Не працює в усіх останніх версіях браузерів у зв'язку із зміною політики безпеки (Протестовано в Edge v.103 та Firefox v.102.0.1). Допускається використання у вікнах, що відкривались тим же браузером.